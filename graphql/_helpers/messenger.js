/* eslint-disable no-unreachable */

const AWS = require('aws-sdk')

module.exports = class Messenger {
  /*
    envio de e-mail funcional, porém desativado, com respostas dummy,
    basta informar as chaves AWS corretas
  */
  constructor() {
    AWS.config.update({
      accessKeyId: process.env.SNS_ACCESS_KEY,
      secretAccessKey: process.env.SNS_SECRET_KEY,
      region: process.env.AWS_REGION,
    })
  }

  async sendEmail(from, to, htmlBody, subject, bcc) {
    return '637b63cf-2e29-47b3-a00c-26b3421382bb';

    if (!from) {
      from = 'noreply@escambau.com.br'
    }

    if (!bcc || process.env.NODE_ENV !== 'production') {
      bcc = []
    }

    const params = {
      Destination: {
        ToAddresses: to,
        BccAddresses: bcc,
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: htmlBody,
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject,
        },
      },
      Source: from,
    };

    const result = await new AWS.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
    return result.MessageId;
  }

  async sendSms(phoneNumber, message) {
    return '27bc4e2b-8cb5-4518-a071-c4f8a4248bbf'

    if (process.env.SMS_PWD_RECOVERY !== 'true') {
      return false
    }

    const sns = new AWS.SNS({ apiVersion: '2010-03-31' })

    const params = {
      PhoneNumber: phoneNumber,
      Message: message,
      MessageStructure: 'string',
    }

    sns.publish(params, (err, data) => {
      if (err) {
        throw err
      } else {
        return data.MessageId
      }
    });
  }
}
