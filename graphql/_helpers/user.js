const userModule = require('../modules/User')
const bookModule = require('../modules/Book')

module.exports = {
  // insersão de usuário - rota livre
  async userCreate(req) {
    try {
      // verifica se o usuário consta na base de dados
      const users = await userModule.getUsers(req.body.email);
      if (users.length > 0) {
        return { status: false, message: 'Usuário já cadastrado' }
      }

      const result = await userModule.userCreate(req.body)

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
  // alteração de usuário - somente admin
  async userUpdate(req) {
    try {
      // verifica se o livro consta na base de dados
      const book = await bookModule.getBook(req.body.book_id);
      if (!book) {
        return { status: false, message: 'Livro não encontrado' }
      }

      // verifica se o isbn já pertence à outro livro
      const books = await bookModule.getBooksByIsbn(req.body.isbn, book.id)
      if (books.length > 0) {
        return { status: false, message: 'O ISBN informado pertente à outro livro' }
      }

      const resultBook = await bookModule.bookUpdate(book.id, req.body)

      return { status: true, result: { resultBook } }
    } catch (error) {
      throw new Error(error)
    }
  },
}
