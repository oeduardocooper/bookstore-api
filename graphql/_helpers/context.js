/* eslint-disable no-unused-vars */
const jwt = require('jsonwebtoken');
const { getUser } = require('../modules/User')
const { getErrorItem } = require('./errorList')

module.exports = async ({ req, connection }) => {
  if (connection) {
    // verifica a metadata da conexão
    return connection.context;
  }
  const auth = req.headers.authorization
  const token = auth && auth.replace('Bearer ', '');
  let user = null
  if (token) {
    try {
      const contentToken = jwt.decode(token, process.env.APP_AUTH_SECRET);
      const checkUser = await getUser(contentToken.data.id)
      if (checkUser && contentToken !== null) {
        user = contentToken;
      }
    } catch (e) {
      // token inválido
      throw new Error(e)
    }
  }

  const err = new Error(getErrorItem('access_denied'))

  return {

    validateUser(permissionName) {
      // implementar de acordo com a forma de gerenciamento de permissões
      // console.log(permissionName)
      // usuário do token
      // console.log(user)
      if (!user) {
        throw new Error(getErrorItem('access_denied'))
      }
      return user
    },
    isAdmin() {
      // implementar de acordo com a forma de gerenciamento de permissões
      return true
    },
  }
}
