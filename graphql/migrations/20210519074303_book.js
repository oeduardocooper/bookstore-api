const defaultColumn = require('./functions/defaultColumns')

exports.up = function (knex) {
    return knex.schema.createTable('book', (table) => {
      table.increments('id').primary();
      table.string('name').notNull();
      table.string('isbn').notNull();
      defaultColumn(knex, table)
    })
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('book');
  };
  