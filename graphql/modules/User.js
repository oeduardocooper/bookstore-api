const config = require('../knexfile.js')
const knex = require('knex')(config)
const moment = require('moment')
const CryptoJS = require('crypto-js')

module.exports = {

  async authUserByEmail(email) {
    return knex.select(
      'user.id',
      'user.name',
      'user.email',
      'user.password',
      'user.is_admin',
    ).from('user')
      .first()
      .where({ 'user.email': email })
  },

  async getUsers(search, perPage, currentPage, pagination = false, order = null) {
    // ordenação padrão, caso não seja informada
    if (!order) {
      order = { column: 'user.name', order: 'asc' }
    }

    // a paginação não é obrigatória
    if (!perPage || !currentPage) {
      perPage = 9999
      currentPage = 1
    }
    const res = await knex('user')
      .select(
        'user.*',
      )
      .modify((queryBuilder) => {
        if (search) {
          queryBuilder
            .andWhere((builder) => builder
              .where('user.name', 'like', `%${search}%`)
              .orWhere('user.email', 'like', `%${search}%`))
        }
      })
      .orderBy(order.column, order.order)
      .paginate({ perPage, currentPage })

    if (pagination) {
      return res
    }
    return res.data
  },

  getUser(id) {
    try {
      return knex('user')
        .select(
          'user.*',
        )
        .first()
        .where({ 'user.id': id })
    } catch (err) {
      throw new Error(err)
    }
  },

  async userCreate(data) {
    data.password = CryptoJS.AES.encrypt(data.password, process.env.SECRET_PASS_KEY).toString()

    const result = await knex('user')
      .returning('*')
      .insert(data)
      .then(async (response) => response)
      .catch((err) => {
        throw err;
      })

    delete result[0].password

    return result
  },

  async userUpdate(id, data, ctx) {
    try {
      const { data: { id: updated_by } } = ctx.validateUser();

      const params = {
        ...data,
        updated_by,
        updated_at: moment.utc().format(),
      }

      return knex('user')
        .where({ id })
        .update(params, '*')
    } catch (err) {
      throw new Error(err)
    }
  },

  async internalUserUpdate(id, data) {
    try {
      const params = {
        ...data,
        updated_at: moment.utc().format(),
      }
      return knex('user')
        .where({ id })
        .update(params, '*')
    } catch (err) {
      throw new Error(err)
    }
  },

  userDelete(id) {
    try {
      return knex('user')
        .where({ id })
        .del()
    } catch (err) {
      throw new Error(err)
    }
  },

}
