const CryptoJS = require('crypto-js');

exports.seed = function (knex) {
  return knex('user').del()
    .then(() =>
      knex('user').insert([
        {
          name: 'Teste Madeira Madeira',
          email: 'testebackend@madeiramadeira.com.br',
          password: CryptoJS.AES.encrypt('testemadeiramadeira', process.env.SECRET_PASS_KEY).toString(),
          is_admin: true,
        },
      ])
    )
};
