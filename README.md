# Bem vindo à API do projeto Bookstore-api!


# A API

Desenvolvida utilizando **Apollo Server** - framework GraphQL, que roda junto com o Express utilizando o módulo **apollo-server-express**, o arquivo de configuração principal é o **server.js**
- A API roda em uma máquina **docker**
- O banco de dados também fica em uma máquina **docker**
- O PgAdmin também fica em uma máquina **docker**
- As variáveis de ambiente encontram-se divididas em arquivos distintos para desenvolvimento, teste e produção
- O ORM utilizado é o **Knex**. Responsável por gerenciar a estrutura do banco de dados, alimentar as tabelas inicialmente. Nada deve ser criado diretamente no banco de dados, qualquer alteração deve ser feita via mutation.
- ESLint é utilizado para manter padronização no código fonte, não permite commit que não se adeque às regras.
- ValidatorJs é utilizado para validar os payloads das requisições.


## Estrutura de arquivos e pastas
>  **_helpers**
> Estruturas de código que podem ser utilizadas em vários pontos do projeto

> **migrations**
> Os arquivos aqui contidos são gerados automaticamente, um migration é criado pelo seguinte comando:$ npx knex migrate:make nomedatabela_table

> **modules**
> São os arquivos responsáveis por "conversar" com o banco de dados. Nenhuma requisição ao banco de dados deve ser realizada fora deste local

## Executando a aplicação

### para o projeto rodar localmente:

$ yarn install (executar dentro da pasta graphql)
>**_instala as dependências_**

$ make dev (na raiz do projeto)
>**_executa e roda o projeto localmente_**

$ make reset (na raiz do projeto)
>**Cria a base de dados e a popula com os seeds**

$ make logs bookstore_graphql (na raiz do projeto)
>**_exibe os logs em tempo de execução_**

### outros comandos:

$ make build (na raiz do projeto)
>**_Executa o build do projeto_**

$ make up (na raiz do projeto)
 >**_Executa o projeto_**

$ make stop (na raiz do projeto)
>**_Pára o projeto_**

$ make migrate (na raiz do projeto)
>**_Cria a base de dados_**

### GraphQL Apollo Server
> **url:** http://localhost:4000/graphql
> **consultar os payloads no fim deste documento**

### Postgres Admin
> **url:** http://localhost:16543/login
> **login:** dev@bookstore.com.br
> **password:** bookstore
>

 **_configurações do painel do Postgres Admin:_**
 > **host:** bookstore_postgres
> **database:** bookstore
>**username:** bookstore
>**password:** bookstore

## Payloads do Postman
Os payloads encontram-se na raiz deste projeto:

> bookstore.postman_collection.json
