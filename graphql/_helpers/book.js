const bookModule = require('../modules/Book')
const categoryModule = require('../modules/Category')
const userBookModule = require('../modules/UserBook')

module.exports = {
  // insersão de livro - somente admin
  async bookCreate(req, user) {
    try {
      // verifica se o livro já consta na base de dados
      const books = await bookModule.getBooks(req.body.isbn);
      if (books.length > 0) {
        return { status: false, message: 'Livro já cadastrado' }
      }

      const resultBook = await bookModule.bookCreate(req.body, user)
      if (!user.is_admin) {
        const resultUserBook = await userBookModule.userBookCreate(req.body, resultBook[0], user)
        return { status: true, resultBook, resultUserBook }
      }

      return { status: true, resultBook }
    } catch (error) {
      throw new Error(error)
    }
  },
  // alteração de livro - somente admin
  async bookUpdate(req, user) {
    try {
      // verifica se o livro consta na base de dados
      const book = await bookModule.getBook(req.body.book_id);
      if (!book) {
        return { status: false, message: 'Livro não encontrado' }
      }

      // verifica se o isbn já pertence à outro livro
      if (req.body.isbn) {
        const books = await bookModule.getBooksByIsbn(req.body.isbn, book.id)
        if (books.length > 0) {
          return { status: false, message: 'O ISBN informado pertente à outro livro' }
        }
      }

      const result = await bookModule.bookUpdate(book.id, req.body, user)

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
  // lista de livros - publico
  async bookList() {
    try {
      const result = await bookModule.getBooks()

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
  // link de usuários livros e categorias - público controlado
  async bookLink(req, user) {
    try {
      // verifica se o livro consta na base de dados
      const book = await bookModule.getBook(req.body.book_id);
      if (!book) {
        return { status: false, message: 'Livro não encontrado' }
      }

      // verifica se a categoria consta na base de dados
      const category = await categoryModule.getCategory(req.body.category_id);
      if (!category) {
        return { status: false, message: 'Categoria não encontrada' }
      }

      // verifica se o livro já foi vinculado ao usuário anteriormente
      const userBook = await userBookModule.getUserBook(book, user)
      if (userBook) {
        return { status: false, message: 'Livro já vinculado ao usuário' }
      }

      const result = await userBookModule.userBookCreate(req.body, book, user)

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
  // unlink de usuários livros e categorias - público controlado
  async bookUnLink(req, user) {
    try {
      // verifica se o livro consta na base de dados
      const book = await bookModule.getBook(req.body.book_id);
      if (!book) {
        return { status: false, message: 'Livro não encontrado' }
      }

      // verifica se o livro já foi vinculado ao usuário anteriormente
      const userBook = await userBookModule.getUserBook(book, user)
      if (!userBook) {
        return { status: false, message: 'Livro não vinculado ao usuário' }
      }

      const result = await userBookModule.userBookDelete(userBook)

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
  // lista de livros do usuário - publico controlado
  async myBookList(user) {
    try {
      const result = await userBookModule.getUserBooks(user)

      return { status: true, result }
    } catch (error) {
      throw new Error(error)
    }
  },
}
