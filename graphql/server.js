const http = require('http');
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const { importSchema } = require('graphql-import');
const logger = require('pino')()
const resolvers = require('./resolvers');

const schemaPath = './schema/index.graphql'
const cookieParser = require('cookie-parser')
const context = require('./_helpers/context')
const routes = require('./routes/routes')

// paginação
const { attachPaginate } = require('knex-paginate');

attachPaginate();

const server = new ApolloServer({
  cors: false,
  typeDefs: importSchema(schemaPath),
  resolvers,
  apollo: {
    key: process.env.APOLLO_KEY,
    graphVariant: process.env.NODE_ENV,
  },
  subscriptions: {
    path: '/subscription',
  },
  formatError: (error) => {
    logger.error(error)
    const newError = {
      name: error.extensions.code || 'API_ERROR',
      message: error.message,
      i18n: error.extensions.exception.constraint || 'default_error',
      statusCode: 400,
    };
    return newError;
  },
  formatResponse: (response) => response,
  context,
  tracing: false,
  introspection: true,
});

const app = express();
const path = '/'
app.use(express.json());
routes(app);

app.use(cookieParser())
app.use(express.urlencoded({ extended: true }))

server.applyMiddleware({ app, path });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: 4000 }, () => {
  // eslint-disable-next-line no-console
  console.log('version 19.0.0')
  // eslint-disable-next-line no-console
  console.log(`🚀 Server teste ready at http://localhost:4000${server.graphqlPath}`)
  // eslint-disable-next-line no-console
  console.log(`🚀 Subscriptions ready at ws://localhost:4000${server.subscriptionsPath}`)
});
