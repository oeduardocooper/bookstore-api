const jwt = require('jsonwebtoken');
const { getUser } = require('../modules/User');

module.exports = {

  async validateToken(req, adminArea = true) {
    const auth = req.headers.authorization
    const token = auth && auth.replace('Bearer ', '');

    // token não informado
    if (!token) {
      return false
    }

    const contentToken = jwt.decode(token, process.env.APP_AUTH_SECRET);
    const user = await getUser(contentToken.data.id)

    // token válido, porém o usuário não existe na base de dados
    if (!user) {
      return false
    }

    // rota reservada somente para admin
    if (user.is_admin !== adminArea) {
      return false
    }

    return user
  },
}
