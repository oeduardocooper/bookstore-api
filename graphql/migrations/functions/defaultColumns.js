module.exports = (knex, table) => {
  table.boolean('enabled').notNull().default(true);
  table.integer('created_by').unsigned()
  table.foreign('created_by').references('user.id').onDelete('CASCADE')
  table.integer('updated_by').unsigned();
  table.foreign('updated_by').references('user.id').onDelete('CASCADE')
  table.timestamp('created_at').defaultTo(knex.fn.now());
  table.timestamp('updated_at');
  return table
}
