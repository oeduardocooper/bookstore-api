const config = require('../knexfile.js')
const knex = require('knex')(config)

module.exports = {

  async userBookCreate(data, book, user) {
    const params = {
      user_id: user.id,
      book_id: book.id,
      category_id: data.category_id,
      created_by: user.id,
    }

    return knex('user_book')
      .returning('*')
      .insert(params)
      .then(async (response) => response)
      .catch((err) => {
        throw err;
      })
  },

  async getUserBook(book, user) {
    try {
      return knex('user_book')
        .select(
          'user_book.*',
        )
        .first()
        .where({ 'user_book.user_id': user.id, 'user_book.book_id': book.id })
    } catch (err) {
      throw new Error(err)
    }
  },

  userBookDelete(userBook) {
    try {
      return knex('user_book')
        .where({ id: userBook.id })
        .del();
    } catch (err) {
      throw new Error(err)
    }
  },

  async getUserBooks(user) {
    try {
      const res = await knex.select(
        'book.id as book_id',
        'book.name as book_name',
        'book.isbn',
        'category.name as category',
      )
        .from('user_book')
        .innerJoin('book', 'book.id', 'user_book.book_id')
        .innerJoin('category', 'category.id', 'user_book.category_id')
        .where({ 'user_book.user_id': user.id })
      return res
    } catch (err) {
      throw new Error(err)
    }
  },

}
