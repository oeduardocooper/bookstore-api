const defaultColumn = require('./functions/defaultColumns')

exports.up = function (knex) {
    return knex.schema.createTable('user_book', (table) => {
      table.increments('id').primary();
      table.integer('user_id')
        .notNull()
        .unsigned();
      table.foreign('user_id')
        .references('user.id')
        .onDelete('CASCADE');
      table.integer('book_id')
        .notNull()
        .unsigned();
      table.foreign('book_id')
        .references('book.id')
        .onDelete('CASCADE');     
      table.integer('category_id')
        .notNull()
        .unsigned();
      table.foreign('category_id')
        .references('category.id')
        .onDelete('CASCADE');                
      table.unique(['user_id', 'book_id', 'category_id'])
      defaultColumn(knex, table)
    })
  };
  
  exports.down = function (knex) {
    return knex.schema.dropTable('user_book');
  };