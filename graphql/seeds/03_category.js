exports.seed = function (knex) {
  return knex('category').del()
    .then(() =>
      knex('category').insert([
        {
          name: 'Lendo',
        },
        {
          name: 'Quero Ler',
        },        
        {
          name: 'Lido',
        },        
      ])
    )
};
