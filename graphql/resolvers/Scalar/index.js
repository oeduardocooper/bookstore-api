const GraphQLJSON = require('graphql-type-json');
const DateFormatted = require('./scalarDateFormatted');

module.exports = {
  JSON: GraphQLJSON,
  DateFormatted,
}
