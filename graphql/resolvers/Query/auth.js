const jwt = require('jsonwebtoken')
const CryptoJS = require('crypto-js')
const userModule = require('../../modules/User')
const { getErrorList, getErrorItem } = require('../../_helpers/errorList')

async function generateToken(user) {
  const data = {
    id: user.id,
    name: user.name,
    email: user.email,
  }

  return {
    token: jwt.sign({
      data,
    }, process.env.APP_AUTH_SECRET),
  }
}

module.exports = {
  async auth(_, args) {
    const { data: { email, password } } = args

    const user = await userModule.authUserByEmail(email)

    if (!user) {
      throw new Error(getErrorItem('user_not_found'))
    }

    const decryptPassworld = CryptoJS.AES.decrypt(
      user.password,
      process.env.SECRET_PASS_KEY,
    ).toString(CryptoJS.enc.Utf8)

    if (decryptPassworld !== password) throw new Error(getErrorItem('incorrect_password'))

    return generateToken(user)
  },

  async errorList(source, args, ctx) {
    try {
      ctx && ctx.validateUser()
      return getErrorList()
    } catch (error) {
      throw new Error(error)
    }
  },
}
