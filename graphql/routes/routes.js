/* eslint-disable func-names */
const { validateToken } = require('../_helpers/tokenValidator')
const { getErrorItem } = require('../_helpers/errorList')
const Validator = require('validatorjs');
const { bookCreate, bookUpdate, bookList, bookLink, bookUnLink, myBookList } = require('../_helpers/book')
const { userCreate } = require('../_helpers/user')

module.exports = function (app) {
  // insersão de livro - somente admin
  app.post('/createBook', async (req, res) => {
    // valida o token
    const user = await validateToken(req, true);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    // regras do validator
    const rules = {
      name: 'required:string',
      isbn: 'required:string',
      category_id: 'integer',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await bookCreate(req, user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // alteração de livro -  somente admin - status e demais propriedades
  app.post('/updateBook', async (req, res) => {
    // valida o token
    const user = await validateToken(req, true);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    // regras do validator
    const rules = {
      book_id: 'required:integer',
      name: 'required:string',
      isbn: 'required:string',
      enabled: 'required:boolean',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await bookUpdate(req, user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // ativação e desativação de livro -  somente admin -
  app.post('/changeStatusBook', async (req, res) => {
    // valida o token
    const user = await validateToken(req, true);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    // regras do validator
    const rules = {
      book_id: 'required:integer',
      enabled: 'required:boolean',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await bookUpdate(req, user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // insersão de usuário - rota pública
  app.post('/createUser', async (req, res) => {
    // regras do validator
    const rules = {
      name: 'required:string',
      email: 'required:email',
      password: 'required:string',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await userCreate(req)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // lista de livros - rota publica
  app.get('/listBooks', async (req, res) => {
    const result = await bookList(req)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // vínculo de livros e usuários - rota pública controlada
  app.post('/linkBook', async (req, res) => {
    // valida o token
    const user = await validateToken(req, false);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    // regras do validator
    const rules = {
      book_id: 'required:integer',
      category_id: 'required:integer',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await bookLink(req, user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // vínculo de livros e usuários - rota pública controlada
  app.post('/unlinkBook', async (req, res) => {
    // valida o token
    const user = await validateToken(req, false);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    // regras do validator
    const rules = {
      book_id: 'required:integer',
    }

    const validation = new Validator(req.body, rules)

    if (!validation.passes()) {
      res.status(400)
      res.send(validation.errors.all())
      return
    }

    const result = await bookUnLink(req, user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });

  // lista de livros vinculados ao usuário
  app.get('/myBookList', async (req, res) => {
    // valida o token
    const user = await validateToken(req, false);

    if (!user) {
      res.status(401)
      res.send(getErrorItem('access_denied'))
      return
    }

    const result = await myBookList(user)

    if (!result.status) {
      res.status(400)
      res.send(result)
      return
    }

    res.send(result)
  });
}
