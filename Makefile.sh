ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(ARGS):;@:)

MAKEFLAGS += --silent

build:
	docker-compose build --no-cache --pull

up:
	cd graphql && yarn install && cd .. && docker-compose up -d --force-recreate && docker-compose exec bookstore_graphql npx knex migrate:latest
	make ips

dev:
	docker-compose -f docker-compose.yml -f dev-docker-compose.yml up -d --force-recreate

stop:
	docker-compose stop

logs:
	docker-compose logs --follow $(ARGS)

ips:
	docker-compose ps

migrate:
	docker-compose exec bookstore_graphql npx knex migrate:latest

reset:
	docker-compose exec bookstore_graphql npx knex migrate:rollback --all && docker-compose exec bookstore_graphql npx knex migrate:latest && docker-compose exec bookstore_graphql npx knex seed:run
