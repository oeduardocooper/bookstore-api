const Query = require('./Query');
const { JSON, DateFormatted } = require('./Scalar');

module.exports = {
  Query,
  JSON,
  DateFormatted,
};
