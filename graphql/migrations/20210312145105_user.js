const defaultColumn = require('./functions/defaultColumns')

exports.up = function (knex) {
  return knex.schema.createTable('user', (table) => {
    table.increments('id').primary();
    table.string('name').notNull();
    table.string('email').unique();
    table.string('password').notNull();
    table.boolean('is_admin').default(false);
    defaultColumn(knex, table)
  })
};

exports.down = function (knex) {
  return knex.schema.dropTable('user');
};
