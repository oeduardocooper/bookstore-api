const config = require('../knexfile.js')
const knex = require('knex')(config)
const moment = require('moment')

module.exports = {

  async getBooks(search, perPage, currentPage, pagination = false, order = null, enabled = true) {
    // ordenação padrão, caso não seja informada
    if (!order) {
      order = { column: 'book.name', order: 'asc' }
    }

    // a paginação não é obrigatória
    if (!perPage || !currentPage) {
      perPage = 9999
      currentPage = 1
    }
    const res = await knex('book')
      .select(
        'book.*',
      )
      .modify((queryBuilder) => {
        if (search) {
          queryBuilder
            .andWhere((builder) => builder
              .where('book.name', 'like', `%${search}%`)
              .orWhere('book.isbn', 'like', `%${search}%`))
        }
        if (enabled) {
          queryBuilder
            .andWhere((builder) => builder
              .where('book.enabled', true))
        }
      })
      .orderBy(order.column, order.order)
      .paginate({ perPage, currentPage })

    if (pagination) {
      return res
    }
    return res.data
  },

  getBook(id) {
    try {
      return knex('book')
        .select(
          'book.*',
        )
        .first()
        .where({ 'book.id': id })
    } catch (err) {
      throw new Error(err)
    }
  },

  getBooksByIsbn(isbn, id) {
    try {
      return knex('book')
        .select(
          'book.*',
        )
        .where({ 'book.isbn': isbn })
        .andWhereNot({ 'book.id': id })
    } catch (err) {
      throw new Error(err)
    }
  },

  async bookCreate(data, user) {
    const params = {
      name: data.name,
      isbn: data.isbn,
      created_by: user.id,
    }

    const result = await knex('book')
      .returning('*')
      .insert(params)
      .then(async (response) => response)
      .catch((err) => {
        throw err;
      })
    return result
  },

  async bookUpdate(id, data, user) {
    try {
      const params = {
        name: data.name,
        isbn: data.isbn,
        enabled: data.enabled,
        updated_by: user.id,
        updated_at: moment.utc().format(),
      }

      return knex('book')
        .where({ id })
        .update(params, '*')
    } catch (err) {
      throw new Error(err)
    }
  },
}
