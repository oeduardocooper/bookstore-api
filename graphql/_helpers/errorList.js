const errorList = [

  { entity: 'global', code: '0000', alias: 'access_denied', message: 'you don\'t have permission to access this area' },

  { entity: 'user', code: '0001', alias: 'user_not_found', message: 'user not found' },
  { entity: 'user', code: '0002', alias: 'incorrect_password', message: 'the password is incorrect' },
  { entity: 'user', code: '0003', alias: 'admin_user_only', message: 'only admin in this area' },
  { entity: 'user', code: '0004', alias: 'email_not_found', message: 'user\'s e-mail not found' },
]

module.exports = {
  getErrorList() {
    return errorList
  },
  getErrorItem(alias, asString = true) {
    let found = errorList.find((element) => element.alias === alias);
    if (!found) {
      found = { entity: 'any', code: '9999', alias: 'generic_error' }
    }
    if (asString) {
      return JSON.stringify(found)
    }
    return found
  },
}
