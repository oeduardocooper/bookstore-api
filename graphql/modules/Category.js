const config = require('../knexfile.js')
const knex = require('knex')(config)

module.exports = {

  async getCategories(search, perPage, currentPage, pagination = false, order = null) {
    // ordenação padrão, caso não seja informada
    if (!order) {
      order = { column: 'book.name', order: 'asc' }
    }

    // a paginação não é obrigatória
    if (!perPage || !currentPage) {
      perPage = 9999
      currentPage = 1
    }
    const res = await knex('category')
      .select(
        'category.*',
      )
      .modify((queryBuilder) => {
        if (search) {
          queryBuilder
            .andWhere((builder) => builder
              .where('category.name', 'like', `%${search}%`))
        }
      })
      .orderBy(order.column, order.order)
      .paginate({ perPage, currentPage })

    if (pagination) {
      return res
    }
    return res.data
  },

  getCategory(id) {
    try {
      return knex('category')
        .select(
          'category.*',
        )
        .first()
        .where({ 'category.id': id })
    } catch (err) {
      throw new Error(err)
    }
  },
}
