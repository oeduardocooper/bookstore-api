exports.seed = function (knex) {
  return knex('book').del()
    .then(() =>
      knex('book').insert([
        {
          name: 'Noite na Taverna',
          isbn: '9788574922492',
          enabled: true,
        },
        {
          name: 'Lira dos Vinte Anos',
          isbn: '9788538003564',
          enabled: true,
        },        
        {
          name: 'Macario',
          isbn: '9788538003564',
          enabled: true,
        },    
        {
          name: 'Poemas Malditos',
          isbn: '8526500783',
          enabled: true,
        },
        {
          name: 'A Dança dos Ossos',
          isbn: '9788575126660',
          enabled: true,
        },
        {
          name: 'O Seminarista',
          isbn: '9788562069659',
          enabled: true,
        },
        {
          name: 'As Trevas e Outros Poemas',
          isbn: '9788502067226',
          enabled: true,
        },
        {
          name: 'O Corsário',
          isbn: '9788569199038',
          enabled: true,
        },
        {
          name: 'Eu e Outras Poesias',
          isbn: '8533602936',
          enabled: true,
        },
        {
          name: 'Clepsidra',
          isbn: '9788574804620',
          enabled: true,
        },
        {
          name: 'O Spleen de Paris',
          isbn: '9788525437549',
          enabled: true,
        },
        {
          name: 'Flores do Mal',
          isbn: '8520503438',
          enabled: true,
        },
        {
          name: 'Sobre a Modernidade',
          isbn: '8520503438',
          enabled: true,
        },
        {
          name: 'Broquéis Faróis',
          isbn: '9788565965774',
          enabled: true,
        },
        {
          name: 'Últimos Sonetos',
          isbn: '9788589072441',
          enabled: true,
        },
      ])
    )
};
