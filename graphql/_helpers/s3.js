const AWS = require('aws-sdk')
const config = require('./awsConfig');

module.exports = new AWS.S3(config.s3);
